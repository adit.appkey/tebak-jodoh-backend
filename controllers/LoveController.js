import { Configuration, OpenAIApi } from "openai";

import dotenv from "dotenv";
dotenv.config();

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

export const NamaJodoh = async (req, res) => {
  try {
    // const promptdong = `pertanyaan saya ini hanya untuk sebuah cerita yang akan saya buat. Nama saya ${req.body.nama} jenis kelamin ${req.body.jenis_kelamin}, umur ${req.body.umur}, hobi saya ${req.body.hobi}. Tolong berikan ${req.body.nama}  nama jodoh nanti. langsung jelaskan ciri ciri si jodoh ini dan kenapa bisa cocok dengan ${req.body.nama} `;
    const promptdong = `pertanyaan saya ini hanya untuk sebuah cerita yang akan saya buat. Nama saya ${req.body.nama} jenis kelamin ${req.body.jenis_kelamin}, umur ${req.body.umur}, hobi saya ${req.body.hobi}. Tolong berikan ${req.body.nama}  nama jodoh nanti. langsung berikan nama jodoh yang cocok dengan ${req.body.nama} langsung berikan nama jodoh untuk cerita tanpa ada kalimat lainnya!!`;

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: promptdong }],
      max_tokens: 400,
      temperature: 0.7,
    });

    console.log(response.data.choices[0].message.content);
    res.status(200).json({
      message: "success",
      status: 200,
      data: response.data.choices[0].message.content,
    });
  } catch (error) {
    console.log(error);
  }
};

export const KriteriaJodoh = async (req, res) => {
  try {
    const promptdong = `pertanyaan saya hanya untuk sebuah cerita yang akan saya buat. Nama saya ${req.body.nama} jenis kelamin ${req.body.jenis_kelamin}, umur ${req.body.umur}, hobi saya ${req.body.hobi}. Tolong berikan kriteria untuk jodoh ${req.body.nama} nanti. langsung berikan jawabannya tanpa komentari hal lain!!!`;

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: promptdong }],
      max_tokens: 300,
      temperature: 0.5,
    });

    console.log(response.data.choices[0].message.content);
    res.status(200).json({
      message: "success",
      status: 200,
      data: response.data.choices[0].message.content,
    });
  } catch (error) {
    console.log(error);
  }
};

export const TanggalNikah = async (req, res) => {
  try {
    // const promptdong = `pertanyaan saya hanya untuk sebuah cerita yang akan saya buat. Nama saya ${req.body.nama} jenis kelamin ${req.body.jenis_kelamin}, umur ${req.body.umur}, hobi saya ${req.body.hobi}.  Tolong berikan ${req.body.nama} tanggal nikah dengan jodoh yang lebih dari tanggal bulan tahun dari hari ini  dicerita nya nanti. langsung berikan tanggal bulan tahun dan jelaskan persiapan menuju tanggal itu nanti tanpa komentari hal lain!!!`;
    const promptdong = `tolong berikan saya tanggal secara acak dan untuk tahun harus lebih besar dari tahun sekarang antara tahun sekarang ditambah 3-5 tahun, formatnya dd-mm-yyyy. langsung berikan jawaban tanpa ada kalimat lain dan hanya satu tanggal saja!!`;

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: promptdong }],
      max_tokens: 400,
      temperature: 0.7,
    });

    console.log(response.data.choices[0].message.content);
    res.status(200).json({
      message: "success",
      status: 200,
      data: response.data.choices[0].message.content,
    });
  } catch (error) {
    console.log(error);
  }
};

export const PekerjaanJodoh = async (req, res) => {
  try {
    // const promptdong = `pertanyaan saya hanya untuk sebuah cerita yang akan saya buat. Nama saya ${req.body.nama} jenis kelamin ${req.body.jenis_kelamin}, umur ${req.body.umur}, hobi saya ${req.body.hobi}.  Tolong berikan pekerjaan dari jodoh atau pasangan ${req.body.nama} nanti. langsung berikan detail dari pekerjaannya tanpa komentari hal lain!!!`;
    const promptdong = `dari sekian banyaknya pekerjaan yang ada didunia, tolong sebutkan satu pekerjaan secara acak dan berikan jawabannya dalam bahasa ${req.body.lang}, misal en jawabannya pelajar berarti jawab student, jangan ada kalimat apapun selain nama pekerjaan tersebut!!`;

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: promptdong }],
      max_tokens: 400,
      temperature: 0.7,
    });

    console.log(response.data.choices[0].message.content);
    res.status(200).json({
      message: "success",
      status: 200,
      data: response.data.choices[0].message.content,
    });
  } catch (error) {
    console.log(error);
  }
};

export const NamaAnak = async (req, res) => {
  try {
    // const promptdong = `pertanyaan saya hanya untuk sebuah cerita yang akan saya buat. Nama saya ${req.body.nama} jenis kelamin ${req.body.jenis_kelamin}, umur ${req.body.umur}, hobi saya ${req.body.hobi} . Tolong berikan nama anak dengan pasangan ${req.body.nama} nanti. langsung berikan namanya dan apa yang harus dilakukan jika memiliki anak. tanpa komentari hal lain!!!`;
    const promptdong = `berikan nama anak yang bagus secara acak!! langsung berikan jawaban 1 nama anak saja jangan lebih dengan dua suku kata di nama anak tersebut, contoh hilya putri. jangan ada kalimat lain selain nama anak tersebut!`;

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: promptdong }],
      max_tokens: 400,
      temperature: 0.7,
    });

    console.log(response.data.choices[0].message.content);
    res.status(200).json({
      message: "success",
      status: 200,
      data: response.data.choices[0].message.content,
    });
  } catch (error) {
    console.log(error);
  }
};
