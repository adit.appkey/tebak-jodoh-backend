import express from "express";
import LoveRoute from "./routes/LoveRoute.js";
import TruthDareRoute from "./routes/TruthDareRoute.js";
import CookingRoute from "./routes/CookingRoute.js";
import cors from "cors";

const app = express();
app.use(cors());
app.use(express.json());
app.use(LoveRoute);
app.use(TruthDareRoute);
app.use(CookingRoute);

app.listen(3452, () => console.log("Server up and running..."));
