import { Configuration, OpenAIApi } from "openai";

import dotenv from "dotenv";
dotenv.config();

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

export const TruthCard = async (req, res) => {
  try {
    const language = req.query.lang;
    const promptdong = `kami sedang bermain game truth or dare, nah tolong buatkan satu hukuman truth secara acak tolong langsung berikan jawabannya tanpa ada kalimat lain selain kalimat truth tersebut dengan bahasa ${language}!!`;

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: promptdong }],
      max_tokens: 400,
      temperature: 0.7,
    });

    console.log(response.data.choices[0].message.content);
    res.status(200).json({
      message: "success",
      status: 200,
      data: response.data.choices[0].message.content,
    });
  } catch (error) {
    console.log(error);
  }
};

export const DareCard = async (req, res) => {
  try {
    const language = req.query.lang;
    const dares = [
      "Lakukan rap gaya bebas selama 3 menit.",
      "Biarkan orang lain membuat status menggunakan akun sosial mediamu.",
      "Berikan ponselmu kepada salah satu di antara kita dan biarkan orang tersebut mengirim satu pesan kepada siapa pun yang dia mau.",
      "Cium salah satu kaus kaki di antara kita.",
      "Makan satu gigitan kulit pisang.",
      "Peragakan salah satu orang di antara kita sampai ada yang bisa menebak siapa orang yang diperagakan.",
      "Katakan ‘”meong” di setiap akhir kalimat sampai giliranmu yang selanjutnya.",
      "Tirukan seorang selebriti sampai ada yang bisa menebak.",
      "Bertingkahlah seperti ayam sampai giliranmu yang selanjutnya.",
      "Biarkan satu orang menggambar tato di wajahmu.",
      "Gunakan penutup mata lalu raba muka salah satu di antara kita sampai kamu bisa menebak siapa orang itu.",
      "Ungkapkan perasaanmu kepada gebetanmu.",
      "Push up 20 kali.",
      "Kayang selama satu menit.",
      "Plank selama satu menit.",
      "Biarkan salah satu di antara kita merias wajahmu menggunakan make up.",
      "Baca dengan lantang pesan yang terakhir kali kamu kirim ke gebetanmu.",
      "Telepon seorang teman dan katakan selamat ulang tahun sambil menyanyikan lagu.",
      "Tunjukkan gerakan dance terbaikmu.",
      "Ulangi setiap perkataan orang yang ada di sampingmu sampai giliranmu yang selanjutnya.",
      "Nyanyikan sebuah lagu dengan namaku.",
      "Bicara dengan gaya yang sangat genit.",
      "Kirim fotomu yang paling jelek.",
      "Beri 10 pujian yang tulus.",
      "Bicara selama 3 menit tanpa berhenti.",
      "Lakukan standup comedy selama 3 menit.",
      "Bisikkan rahasia paling kelam yang kamu sembunyikan.",
      "Tiru 5 emoji.",
      "Tunjukkan seluruh isi tasmu secara detail.",
      "Katakan “aku cinta kamu” sambil push up sebanyak 10 kali.",
      "Tunjukkan riwayat pencarian di ponselmu.",
      "Biarkan aku menggambar tato di tubuhmu.",
      "Biarkan aku memegang ponselmu selama 3 menit.",
      "Teriakkan satu kata yang pertama kali muncul di benakmu.",
      "Katakan 5 hal jujur tentangku.",
      "Kirim gebetanmu selfie yang memalukan.",
      "Menarilah selama satu menit tanpa musik.",
      "Pergi keluar dan berteriaklah sekeras yang kamu bisa.",
      "Ketuk pintu tetangga dan minta telur.",
      "Cobalah tarian TikTok pertama yang muncul di FYP-mu.",
      "Minum segelas air tanpa menggunakan tangan.",
      "Minum kecap langsung dari botolnya.",
      "Makan camilan tanpa menggunakan tangan.",
      "Coba jilat sikumu.",
      "Makan satu bawang mentah.",
      "Pergi keluar dan nyanyikan lagu “Let It Go” dari film Frozen.",
      "Menari balet selama 1 menit.",
      "Hubungi kontak ke-12 di ponselmu dan lakukan percakapan selama 30 detik.",
      "Berbicara dengan suara penyanyi opera selama 4 putaran.",
      "Berbicara dengan mulut tertutup selama 4 putaran.",
      "Nyanyikan lagu rap dengan memasukkan setiap nama pemain.",
      "Menari salsa selama 1 menit.",
      "Like semua foto gebetanmu.",
      "Buat story dengan wajah memelas.",
      "Tunjukkan history pencarian Instagram-mu.",
    ];
    const random = Math.floor(Math.random() * 56);
    const result = dares[random];

    if (language == "id") {
      res.status(200).json({
        message: "success",
        status: 200,
        data: result,
      });
    }
    const promptdong = `${result}. translate kalimatnya ke dalam bahasa ${language}!! dan langsung berikan hasilnya tanpa kalimat lain apapun!!`;

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: promptdong }],
      max_tokens: 400,
      temperature: 0.7,
    });

    console.log(response.data.choices[0].message.content);
    console.log(language);
    res.status(200).json({
      message: "success",
      status: 200,
      data: response.data.choices[0].message.content,
    });
  } catch (error) {
    console.log(error);
  }
};
