import express from "express";
import { ResepMakanan } from "../controllers/CookingController.js";
const router = express.Router();
router.post("/resep-makanan", ResepMakanan);
export default router;