import { Configuration, OpenAIApi } from "openai";

import dotenv from "dotenv";
dotenv.config();

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

export const ResepMakanan = async (req, res) => {
    try {
        const language = 'EN';
        const prompt = `Recipe based on ingredients: ${req.body.bahan}. Translate the answer in ${req.body.lang}`;

        const response = await openai.createChatCompletion({
            model: "gpt-3.5-turbo",
            messages: [{ role: "system", content: "recipe information provider" }, { role: "user", content: prompt }],
            max_tokens: 500,
            frequency_penalty: 0.5,
            presence_penalty: 0.5,
        });        
        
        console.log(response.data.choices[0].message.content);
        res.status(200).json({
            message: "success",
            status: 200,
            data: response.data.choices[0].message.content,
        }); 
    } catch (error) {
        console.log(error);
    }
  };