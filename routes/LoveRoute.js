import express from "express";
import {
  KriteriaJodoh,
  NamaAnak,
  NamaJodoh,
  PekerjaanJodoh,
  TanggalNikah,
} from "../controllers/LoveController.js";
const router = express.Router();
router.post("/", (req, res) => {
  res.status(200).json(req.body.name);

  // code to handle the request and send response
});
router.post("/nama-jodoh", NamaJodoh);
router.post("/tanggal-nikah", TanggalNikah);
router.post("/kriteria-jodoh", KriteriaJodoh);
router.post("/pekerjaan-jodoh", PekerjaanJodoh);
router.post("/nama-anak", NamaAnak);
export default router;
