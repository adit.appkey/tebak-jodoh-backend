import express from "express";
import { DareCard, TruthCard } from "../controllers/TruthDareController.js";

const router = express.Router();

router.get("/truth", TruthCard);
router.get("/dare", DareCard);

export default router;
